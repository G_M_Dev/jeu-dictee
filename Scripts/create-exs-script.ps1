$PSDefaultParameterValues['*:Encoding'] = 'utf8'

function Remove-StringLatinCharacters
{
    PARAM ([string]$String)
    return [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding("Cyrillic").GetBytes($String))
}

$defaultNoise = 'a-z'
Do
{
	$word = Read-Host 'Add this Word'
	if ($word -ne '') {
		$noAccents = Remove-StringLatinCharacters -String $word
		$exsLine = [string]::Format('{0},{1}.wav,{2}', $word, $noAccents, $defaultNoise)
		$exsLine >> ".\exs.csv"
	}
} while ($word -ne '')
