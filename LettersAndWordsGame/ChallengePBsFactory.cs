﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    /// <summary>
    /// Challenge buttons factory class
    /// </summary>
    class ChallengePBsFactory : ButtonsFactory
    {
        private static ChallengePBsFactory cFactoryInstance = null; /// the current factory instance

        /// <summary>
        /// Default constructor
        /// </summary>
        private ChallengePBsFactory():base() { }

        /// <summary>
        /// Get/create the ChallengePBsFactory single instance
        /// </summary>
        /// <returns></returns>
        public static ChallengePBsFactory GetPBFactory()
        {
            if (cFactoryInstance == null)
            {
                cFactoryInstance = new ChallengePBsFactory();
            }
            return cFactoryInstance;
        }

        /// <summary>
        /// Configure the Button based on its type
        /// </summary>
        /// <param name="cPB">Button instance</param>
        /// <param name="ePBType">Button type</param>
        protected override void ConfigTheButton(Button cPB, EventHandler evHdl = null, BUTTON_TYPE ePBType = BUTTON_TYPE.eCHALLENGE)
        {
            if (ePBType != BUTTON_TYPE.eCHALLENGE) { return; }
            int nChlngs = lPBsList.Count;                           // get challenge buttons count
            cPB.Size = new Size(500, 50);                           // size
            cPB.Location = new Point(150, (50 + 20)*nChlngs + 20);  // location
            cPB.Anchor = AnchorStyles.Top;                          // Anchor
            Font cFont = new Font("Arial", 20, FontStyle.Bold);
            cPB.Font = cFont;                                       // font
            cPB.ForeColor = Color.BlueViolet;                       // color
            cPB.Click += evHdl;                                     // event
        }
    }
}
