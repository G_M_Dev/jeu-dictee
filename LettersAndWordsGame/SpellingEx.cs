﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LettersAndWordsGame
{
    class SpellingEx : IDataComponent
    {
        private Challenge cParent;

        private string szText;
        private string szAudio;
        private string szHint;
        private string szNoise;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="szExText">exercice text</param>
        /// <param name="szExAudio">exercice audio file path</param>
        /// <param name="szExNoise">exercice noise letters string</param>
        public SpellingEx(string szExText, string szExAudio, string szExNoise, string szExHint = "NO_HINT", Challenge cPrntChlg = null)
        {
            cParent = cPrntChlg;
            szText = szExText;
            szAudio = szExAudio;
            szHint = szExHint;
            szNoise = szExNoise;
        }

        /// <summary>
        /// returns audio file name
        /// </summary>
        /// <returns>audio file name</returns>
        public string GetAudio()
        {
            return szAudio;
        }

        /// <summary>
        /// returns hint image name
        /// </summary>
        /// <returns>Hint image name</returns>
        public string GetHint()
        {
            return szHint;
        }

        /// <summary>
        /// An individual exercice has no valid name
        /// </summary>
        /// <returns>"?????"</returns>
        public string GetName()
        {
            return "?????";
        }

        /// <summary>
        /// Returns the exercice text
        /// </summary>
        /// <returns></returns>
        public string GetText()
        {
            return szText;
        }


        /// <summary>
        /// returns the exercice noise letters string
        /// </summary>
        /// <returns>exercice associated noise letters string</returns>
        public string GetNoise()
        {
            return szNoise;
        }

        /// <summary>
        /// returns a refernce to itself
        /// </summary>
        /// <returns>a refernce to itself</returns>
        public IDataComponent Load()
        {
            return this;
        }

        /// <summary>
        /// Returns FALSE to tell challenges and Spelling exercices apart
        /// </summary>
        /// <returns>FALSE</returns>
        public bool IsChallenge()
        {
            return false;
        }

        /// <summary>
        /// Gets the component parent ref.
        /// </summary>
        /// <returns>component parent ref</returns>
        public IDataComponent GetParent()
        {
            return cParent;
        }
    }
}
