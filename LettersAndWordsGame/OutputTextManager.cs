﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    /// <summary>
    /// output text manager class
    /// </summary>
    class OutputTextManager
    {
        private static OutputTextManager cOutTextMngr = null; /// the current output text manager instance
        private TextBox cOutTextBox = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        private OutputTextManager()
        {
            if (cOutTextBox == null)
            {
                cOutTextBox = new TextBox();
                cOutTextBox.Name = "OUTPUT_TEXT_BOX";
                cOutTextBox.Text = "";
                cOutTextBox.Size = new Size(500, 100);                  // size
                cOutTextBox.Location = new Point(140, 512);             // location
                cOutTextBox.Anchor = AnchorStyles.Top;                  // Anchor
                Font cFont = new Font("Arial", 40, FontStyle.Bold);
                cOutTextBox.Font = cFont;                               // font
                cOutTextBox.ForeColor = Color.BlueViolet;               // color
                // !!!TODO... set the event: cOutTextBox.Click += evHdl;                                     // event
                cOutTextBox.TabIndex = 0;
            }
        }

        /// <summary>
        /// Get/create the ChallengePBsFactory single instance
        /// </summary>
        /// <returns></returns>
        public static OutputTextManager GetOutTextMngr()
        {
            if (cOutTextMngr == null)
            {
                cOutTextMngr = new OutputTextManager();
            }
            return cOutTextMngr;
        }

        /// <summary>
        /// Adds the output text box to the passed form controls
        /// </summary>
        /// <param name="cControls">controls collection of the form hosting the output text box</param>
        public void AddOutTextBoxToControls(Control.ControlCollection cControls)
        {
            cControls.Add(cOutTextBox);
        }

        /// <summary>
        /// Removes last character in output text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EraseOneChar_Click(object sender, EventArgs e)
        {
            if (cOutTextBox.Text.Length > 0) { cOutTextBox.Text = cOutTextBox.Text.Remove(cOutTextBox.Text.Length - 1); }
        }

        /// <summary>
        /// Erases whole output text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Eraser_Click(object sender, EventArgs e)
        {
            cOutTextBox.Text = string.Empty;
        }

        /// <summary>
        /// Checks given text matches output text
        /// </summary>
        /// <param name="szText">given text to check</param>
        /// <returns>TRUE if given text matches output text, FALSE otherwise</returns>
        public bool Check(string szText)
        {
            return (szText == cOutTextBox.Text);
        }

        /// <summary>
        /// Adds given text to output
        /// </summary>
        /// <param name="szPBText">input text</param>
        public void AddText(string szPBText)
        {
            cOutTextBox.Text += szPBText;
        }

        /// <summary>
        /// Sets right to left property for the output text box
        /// </summary>
        /// <param name="bEnableRightToLeft"></param>
        public void SetRightToLeft(bool bEnableRightToLeft = false)
        {
            cOutTextBox.RightToLeft = bEnableRightToLeft ? RightToLeft.Yes : RightToLeft.No;
        }
    }
}
