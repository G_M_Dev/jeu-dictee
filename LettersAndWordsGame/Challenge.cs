﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LettersAndWordsGame
{
    class Challenge : IDataComponent
    {
        private Challenge cParent;

        private string szName;              // Challenge name
        private List<IDataComponent> cExs;  // Challenge list of exercices / or sub-challenges
        private int nCurrent;               // Current active exercice / or sub-challenge

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="szChallengeName">challenge name</param>
        public Challenge(string szChallengeName, Challenge cPrntChlg = null)
        {
            cParent = cPrntChlg;
            szName = szChallengeName;
            nCurrent = 0;
            cExs = new List<IDataComponent>();
        }

        /// <summary>
        /// Adds a new data component to the list
        /// </summary>
        /// <param name="cDatComp">data component to add</param>
        public void AddToExs(IDataComponent cDatComp)
        {
            cExs.Add(cDatComp);
        }

        /// <summary>
        /// Gets the name of the component
        /// </summary>
        /// <returns>name of the component</returns>
        public string GetName()
        {
            return szName;
        }

        /// <summary>
        /// returns audio source name of the first data component in list
        /// </summary>
        /// <returns>audio source name of the first data component in list</returns>
        public string GetAudio()
        {
            if (nCurrent >= 0)
            {
                return string.Format("{0}/{1}", GetName(), cExs[nCurrent].GetAudio());
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// returns hint image name of the first data component in list
        /// </summary>
        /// <returns>Hint image name of the first data component in list</returns>
        public string GetHint()
        {
            if (nCurrent >= 0)
            {
                return string.Format("{0}/{1}", GetName(), cExs[nCurrent].GetHint());
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// returns text of the first data component in list
        /// </summary>
        /// <returns>text of the first data component in list</returns>
        public string GetText()
        {
            if (nCurrent >= 0)
            {
                return cExs[nCurrent].GetText();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// returns noise letters string of the first data component in list
        /// </summary>
        /// <returns>noise letters string of the first data component in list</returns>
        public string GetNoise()
        {
            if (nCurrent >= 0)
            {
                return cExs[nCurrent]?.GetNoise();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Loads current exercice / or subchallenge in list
        /// </summary>
        /// <returns>current exercice / or subchallenge in list</returns>
        public IDataComponent Load()
        {
            IDataComponent cCompToLoad = ( (nCurrent < cExs.Count)? cExs[nCurrent]: null ); 
            return cCompToLoad;
        }

        /// <summary>
        /// Advances current exercice / subchallenge index
        /// </summary>
        public void Advance()
        {
            if (nCurrent < cExs.Count) { nCurrent++; }
        }

        /// <summary>
        /// Returns TRUE to tell challenges and Spelling exercices apart
        /// </summary>
        /// <returns>TRUE</returns>
        public bool IsChallenge()
        {
            return true;
        }

        /// <summary>
        /// Gets the component parent ref.
        /// </summary>
        /// <returns>component parent ref</returns>
        public IDataComponent GetParent()
        {
            return cParent;
        }

        /// <summary>
        /// returns the current active exercice/subchallenge index
        /// </summary>
        /// <returns></returns>
        public int GetCurrent()
        {
            return nCurrent;
        }

        /// <summary>
        /// Resets the challenge loader
        /// </summary>
        public void ResetLoader()
        {
            if (nCurrent > 0) { nCurrent = 0; }
        }
    }
}
