﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    /// <summary>
    /// Control buttons factory class
    /// </summary>
    class ControlPBsFactory : ButtonsFactory
    {
        private static ControlPBsFactory cFactoryInstance = null; /// the current factory instance

        /// <summary>
        /// Default constructor
        /// </summary>
        private ControlPBsFactory() : base() { }

        /// <summary>
        /// Get/create the ControlPBsFactory single instance
        /// </summary>
        /// <returns></returns>
        public static ControlPBsFactory GetPBFactory()
        {
            if (cFactoryInstance == null)
            {
                cFactoryInstance = new ControlPBsFactory();
            }
            return cFactoryInstance;
        }

        /// <summary>
        /// Configure the Button based on its type
        /// </summary>
        /// <param name="cPB">Button instance</param>
        /// <param name="ePBType">Button type</param>
        protected override void ConfigTheButton(Button cPB, EventHandler evHdl = null, BUTTON_TYPE ePBType = BUTTON_TYPE.eRETURN)
        {
            if (ePBType <= BUTTON_TYPE.eLETTER) { return; }
            cPB.Size = new Size(100, 100);                // size
            switch(ePBType)
            {
                case BUTTON_TYPE.eRETURN:
                    cPB.Location = new Point(20, 20);    // location ; 20
                    break;
                case BUTTON_TYPE.eRESTART:
                    cPB.Location = new Point(170, 20);   // location ; +150
                    break;
                case BUTTON_TYPE.eERASER:
                    cPB.Location = new Point(290, 20);   // location ; +120 
                    break;
                case BUTTON_TYPE.ePENCIL_ERASE:
                    cPB.Location = new Point(410, 20);   // location ; +120
                    break;
                case BUTTON_TYPE.eAUDIO:
                    cPB.Location = new Point(530, 20);   // location ; +120
                    cPB.Text = "1"; cPB.TextAlign = ContentAlignment.BottomRight;
                    break;
                case BUTTON_TYPE.eHINT:
                    cPB.Location = new Point(650, 20);   // location ; +120
                    break;
                case BUTTON_TYPE.eCHECK:
                    cPB.Location = new Point(20, 500);   // location ; 
                    break;
            }
            cPB.Anchor = AnchorStyles.Top;               // Anchor
            cPB.Click += evHdl;        // event
        }

        /// <summary>
        /// Returns a letter button - and adds it at the right position in list
        /// </summary>
        /// <param name="szPBName">Button name - should be one char representing the letter</param>
        /// <param name="evHdl">event handler</param>
        /// <param name="cPBImage">Button image if any</param>
        /// <param name="ePBType">Button type</param>
        /// <returns>Created button</returns>
        public override Button GetButton(string szPBName, EventHandler evHdl, Image cPBImage, BUTTON_TYPE ePBType)
        {
            Button cPB = base.GetButton(szPBName, evHdl, cPBImage, ePBType);
            if (ePBType == BUTTON_TYPE.eAUDIO) { cPB.Text = "1"; }
            return cPB;
        }
    }
}
