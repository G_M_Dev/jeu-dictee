﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    public class ImgMsgBox : Form
    {
        public ImgMsgBox(string szImgPath)
        {
            InitializeComponent(szImgPath);
        }

        /// <summary>
        /// Initializes the Image Message Box component.
        /// </summary>
        private void InitializeComponent(string szImgPath)
        {
            this.SuspendLayout();
            // 
            // Image Message Box
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.AutoScroll = false;
            this.ClientSize = new System.Drawing.Size(350, 334);
            this.Cursor = Cursors.Hand;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ShowIcon = false;
            this.Name = "Image Message Box";
            this.ResumeLayout(false);
            //
            // Image Button
            //
            Button cImgBtn = new Button();
            cImgBtn.BackgroundImage = Image.FromFile(szImgPath);
            cImgBtn.BackgroundImageLayout = ImageLayout.Stretch;
            cImgBtn.Dock = DockStyle.Fill;
            cImgBtn.Click += new EventHandler(Close_ImgBox);
            Controls.Add(cImgBtn);
        }

        /// <summary>
        /// Restarts from first exercice in the current challenge series
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_ImgBox(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
