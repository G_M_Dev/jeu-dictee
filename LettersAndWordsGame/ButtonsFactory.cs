﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    /// <summary>
    ///  Button types enum
    /// </summary>
    enum BUTTON_TYPE
    {
        eCHALLENGE,
        eLETTER,
        ePENCIL_ERASE,
        eERASER,
        eRETURN,
        eRESTART,
        eAUDIO,
        eHINT,
        eCHECK
    }

    /// <summary>
    /// Buttons factory abstract class
    /// </summary>
    abstract class ButtonsFactory
    {
        protected List<Button> lPBsList;  /// list of all buttons created
        
        /// <summary>
        /// Default constructor
        /// </summary>
        protected ButtonsFactory()
        {
            lPBsList = new List<Button>();
        }

        /// <summary>
        /// Creates a new button as described in arguments or returns the matching button if it already exists 
        /// </summary>
        /// <param name="szPBName">Button name - should be one char representing the letter</param>
        /// <param name="evHdl">event handler</param>
        /// <param name="cPBImage">Button image if any</param>
        /// <param name="ePBType">Button type</param>
        /// <returns>Created button</returns>
        public virtual Button GetButton(string szPBName, EventHandler evHdl = null, Image cPBImage = null, BUTTON_TYPE ePBType = BUTTON_TYPE.eCHALLENGE)
        {
            Button cPB = lPBsList.Find(PB => PB.Name == szPBName); // check if button exists
            if (cPB == null) // if NOT create it, configure it and add it to the list
            {
                cPB = new Button();
                // Configure it
                if (cPBImage != null)
                {
                    cPB.BackgroundImage = cPBImage;
                    cPB.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else
                {
                    cPB.Text = szPBName;
                }
                cPB.Name = szPBName;
                ConfigTheButton(cPB, evHdl, ePBType);
                cPB.TabIndex = 0;
                cPB.UseVisualStyleBackColor = true;
                // Add it to the list
                lPBsList.Add(cPB);
            }
            // Return it
            return cPB;
        }

        /// <summary>
        /// Configure the Button based on its type
        /// </summary>
        /// <param name="cPB">Button instance</param>
        /// <param name="ePBType">Button type</param>
        protected abstract void ConfigTheButton(Button cPB, EventHandler evHdl, BUTTON_TYPE ePBType);

        /// <summary>
        /// Deletes all PBs
        /// </summary>
        public void DeleteAllPBs()
        {
            lPBsList.Clear();
        }

        /// <summary>
        /// Returns the list of buttons in an array
        /// </summary>
        /// <returns></returns>
        public Button[] GetButtons()
        {
            return lPBsList.ToArray();
        }
    }
}
