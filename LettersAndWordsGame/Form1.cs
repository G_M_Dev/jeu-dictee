﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace LettersAndWordsGame
{
    public partial class JeuDictee : Form
    {
        private DataReader cGameDataReader;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="szGameDataPath">game data folder path</param>
        public JeuDictee(string szGameDataPath)
        {
            // init
            cGameDataReader = new DataReader(@szGameDataPath);
            if(cGameDataReader.NoData())
            {
                Load += new EventHandler(NoDataAvailable_Load);
            }
            else
            {
                InitializeComponent();
                ShowChallenges(cGameDataReader.GetChallengesNames());
            }
        }

        /// <summary>
        /// Adds challenges buttons
        /// </summary>
        /// <param name="szChallengesNames">challenges names</param>
        private void ShowChallenges(List<string> szChallengesNames)
        {
            if (!szChallengesNames.Any()) { MessageBox.Show("ShowChallenges: No challenges found in here!!"); }
            ChallengePBsFactory cButtonFactory = ChallengePBsFactory.GetPBFactory();
            cButtonFactory.DeleteAllPBs();
            mainPanel.SuspendLayout();
            for(int nChlgNdx = 0; nChlgNdx < szChallengesNames.Count; nChlgNdx++)
            {
                Button cChlgPB = cButtonFactory.GetButton(szChallengesNames[nChlgNdx], new EventHandler(AccessChallenge_Click));
                mainPanel.Controls.Add(cChlgPB);
            }
            mainPanel.ResumeLayout(false);
            mainPanel.PerformLayout();
        }

        /// <summary>
        /// Adds a RETURN button
        /// </summary>
        /// /// <param name="szRESPath">Path to resources folder</param>
        private void AddReturnPB(string szRESPath)
        {
            ControlPBsFactory cButtonFactory = ControlPBsFactory.GetPBFactory();
            OutputTextManager cOutTxtMngr = OutputTextManager.GetOutTextMngr();
            mainPanel.SuspendLayout();
            Button cRETURN = cButtonFactory.GetButton("RETURN",
                        new EventHandler(Return_Click) + new EventHandler(cOutTxtMngr.Eraser_Click),
                        Image.FromFile(string.Format("{0}back_arrow.png", szRESPath)), 
                        BUTTON_TYPE.eRETURN);
            mainPanel.Controls.Add(cRETURN);
            mainPanel.ResumeLayout(false);
        }

        /// <summary>
        /// Builds the default Game Panel
        /// </summary>
        /// <param name="szRESPath">Path to resources folder</param>
        private void BuildGamePanel(string szRESPath)
        {
            ControlPBsFactory cButtonFactory = ControlPBsFactory.GetPBFactory();
            OutputTextManager cOutTxtMngr = OutputTextManager.GetOutTextMngr();
            mainPanel.SuspendLayout();
            // Control buttons
            Button cRESTART = cButtonFactory.GetButton("RESTART",
                        new EventHandler(Restart_Click) + new EventHandler(cOutTxtMngr.Eraser_Click),
                        Image.FromFile(string.Format("{0}restart.png", szRESPath)),
                        BUTTON_TYPE.eRESTART);
            mainPanel.Controls.Add(cRESTART);
            Button cERASER = cButtonFactory.GetButton("ERASER",
                        new EventHandler(cOutTxtMngr.Eraser_Click),
                        Image.FromFile(string.Format("{0}rubber_eraser.png", szRESPath)),
                        BUTTON_TYPE.eERASER);
            mainPanel.Controls.Add(cERASER);
            Button cERASE_ONE_CHAR = cButtonFactory.GetButton("PENCIL_ERASE",
                        new EventHandler(cOutTxtMngr.EraseOneChar_Click),
                        Image.FromFile(string.Format("{0}pencil.png", szRESPath)),
                        BUTTON_TYPE.ePENCIL_ERASE);
            mainPanel.Controls.Add(cERASE_ONE_CHAR);
            Button cAUDIO = cButtonFactory.GetButton("AUDIO",
                        new EventHandler(Audio_Click),
                        Image.FromFile(string.Format("{0}play_audio.png", szRESPath)),
                        BUTTON_TYPE.eAUDIO);
            mainPanel.Controls.Add(cAUDIO);
            Button cHINT = cButtonFactory.GetButton("HINT",
                        new EventHandler(Hint_Click),
                        Image.FromFile(string.Format("{0}give_hint.png", szRESPath)),
                        BUTTON_TYPE.eHINT);
            mainPanel.Controls.Add(cHINT);
            Button cCHECK = cButtonFactory.GetButton("CHECK",
                        new EventHandler(Check_Click),
                        Image.FromFile(string.Format("{0}checkmark.png", szRESPath)),
                        BUTTON_TYPE.eCHECK);
            mainPanel.Controls.Add(cCHECK);
            // Output text box
            cOutTxtMngr.AddOutTextBoxToControls(mainPanel.Controls);
            mainPanel.ResumeLayout(false);
        }

        /// <summary>
        /// displays a No DATA message and ends the game on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NoDataAvailable_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Game cannot start: No DATA found!");
            Close();
        }

        /// <summary>
        /// Accesses the selected challenge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AccessChallenge_Click(object sender, EventArgs e)
        {
            string szPBText = ((Button)sender).Text;
            cGameDataReader.SelectChallenge(szPBText); // make reader point to this challenge
            mainPanel.Controls.Clear(); // clear the current controls
            AddReturnPB(cGameDataReader.GetRESPath());
            if(cGameDataReader.HasExs())
            {
                BuildGamePanel(cGameDataReader.GetRESPath());
                ShowExercice(   cGameDataReader.GetCurrentText(), 
                                cGameDataReader.GetCurrentNoise());
            }
            else
            {
                ShowChallenges(cGameDataReader.GetChallengesNames());
            }
        }

        /// <summary>
        /// Restarts from first exercice in the current challenge series
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Restart_Click(object sender, EventArgs e)
        {
            // !! This function should only execute from a game panel view (when exercices are available)
            cGameDataReader.ResetLoader((Button)mainPanel.Controls.Find("AUDIO", false).First());
            ShowExercice(   cGameDataReader.GetCurrentText(), 
                            cGameDataReader.GetCurrentNoise());
        }

        /// <summary>
        /// Returns backward to top challenge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Return_Click(object sender, EventArgs e)
        {
            cGameDataReader.SelectParent(); // make reader point to the parent challenge
            mainPanel.Controls.Clear(); // clear the current controls
            if (!cGameDataReader.IsSelected("TOP CHALLENGE")) { AddReturnPB(cGameDataReader.GetRESPath()); }
            ShowChallenges(cGameDataReader.GetChallengesNames());
        }

        /// <summary>
        /// Checks the output text matches the current exercice text and if YES moves to next exercice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check_Click(object sender, EventArgs e)
        {
            OutputTextManager cOutTxtMngr = OutputTextManager.GetOutTextMngr();
            if( cOutTxtMngr.Check(cGameDataReader.GetCurrentText()) )
            {
                SoundPlayer goodJobSnd = new SoundPlayer(string.Format("{0}success.wav", cGameDataReader.GetRESPath()));
                goodJobSnd.Play();
                ImgMsgBox cGoodJobBox = new ImgMsgBox( string.Format("{0}good_job.png", cGameDataReader.GetRESPath()) );
                cGoodJobBox.ShowDialog();
                cOutTxtMngr.Eraser_Click(null, null);
                cGameDataReader.LoadNext((Button)mainPanel.Controls.Find("AUDIO",false).First());
                if(cGameDataReader.IsCurrentEx()) {
                    ShowExercice(   cGameDataReader.GetCurrentText(), 
                                    cGameDataReader.GetCurrentNoise());
                }
                else {
                    if(cGameDataReader.GetChallengesNames().Any())
                    {
                        mainPanel.Controls.Clear(); // clear the current controls
                        ShowChallenges(cGameDataReader.GetChallengesNames());
                    }
                    else { Return_Click(null, null); }
                }
            }
            else
            {
                SoundPlayer wrongAnswerSnd = new SoundPlayer(string.Format("{0}wrong_answer.wav", cGameDataReader.GetRESPath()));
                wrongAnswerSnd.Play();
                ImgMsgBox cTryAgainBox = new ImgMsgBox(string.Format("{0}try_again.png", cGameDataReader.GetRESPath()));
                cTryAgainBox.ShowDialog();
            }
        }

        /// <summary>
        /// Plays audio file associated to current exercice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Audio_Click(object sender, EventArgs e)
        {
            string szAudioFile = cGameDataReader.GetCurrentAudio();
            if (File.Exists(@szAudioFile))
            {
                SoundPlayer simpleSound = new SoundPlayer(@szAudioFile);
                simpleSound.Play();
            }
            else { MessageBox.Show("Audio_Click: Audio file not found!"); }
        }

        /// <summary>
        /// Shows hint image associated to current exercice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hint_Click(object sender, EventArgs e)
        {
            string szHintFile = cGameDataReader.GetCurrentHint();
            if (File.Exists(@szHintFile))
            {
                SoundPlayer hint = new SoundPlayer(string.Format("{0}hint.wav", cGameDataReader.GetRESPath()));
                hint.Play();
                ImgMsgBox cHintBox = new ImgMsgBox(szHintFile);
                cHintBox.ShowDialog();
            }
            else {
                SoundPlayer noHint = new SoundPlayer(string.Format("{0}no_hint.wav", cGameDataReader.GetRESPath()));
                noHint.Play();
                ImgMsgBox cNoHintBox = new ImgMsgBox(string.Format("{0}no_hint.png", cGameDataReader.GetRESPath()));
                cNoHintBox.ShowDialog();
            }
        }

        /// <summary>
        /// Adds letters to game panel based on current exercice text and noise string
        /// </summary>
        /// <param name="szText">exercice text</param>
        /// <param name="szNoise">exercice noise string</param>
        private void ShowExercice(string szText, string szNoise)
        {
            string szTextPlusNoise = szNoise + szText;
            LetterPBsFactory cButtonFactory = LetterPBsFactory.GetPBFactory();
            foreach (Button cOldPB in cButtonFactory.GetButtons()) { mainPanel.Controls.Remove(cOldPB); }
            cButtonFactory.DeleteAllPBs();
            mainPanel.SuspendLayout();
            for (int nLttr = 0; nLttr < szTextPlusNoise.Length; nLttr++)
            {
                Button cLttrPB = cButtonFactory.GetButton(szTextPlusNoise.Substring(nLttr, 1), new EventHandler(AddLetter_Click));
            }
            cButtonFactory.PositionLetters();
            mainPanel.Controls.AddRange(cButtonFactory.GetButtons());
            // configure output text manager according to language
            bool bIsArabicEx = (szTextPlusNoise[0] >= 0x0600) && (szTextPlusNoise[0] <= 0x06FF);
            OutputTextManager cOutTxtMngr = OutputTextManager.GetOutTextMngr();
            cOutTxtMngr.SetRightToLeft(bIsArabicEx);
            mainPanel.ResumeLayout(false);
        }

        /// <summary>
        /// Adds the letter to output after a letter button push
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddLetter_Click(object sender, EventArgs e)
        {
            string szPBText = ((Button)sender).Text;
            OutputTextManager cOutTxtMngr = OutputTextManager.GetOutTextMngr();
            cOutTxtMngr.AddText(szPBText);
        }

        /// <summary>
        /// Centers the main panel when window resize detected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CenterMainPanel_Resize(object sender, EventArgs e)
        {
            Form cForm = (Form)sender;
            int nDeltaWidth = cForm.ClientSize.Width - mainPanel.Width;
            if (nDeltaWidth > 0)
            {
                mainPanel.Left = nDeltaWidth / 2;
            }
            else
            {
                mainPanel.Left = 0 + cForm.AutoScrollPosition.X;
            }
            PerformLayout();
        }
    }
}
