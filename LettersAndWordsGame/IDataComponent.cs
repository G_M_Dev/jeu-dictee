﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LettersAndWordsGame
{
    /// <summary>
    /// Data component interface
    /// </summary>
    public interface IDataComponent
    {
        IDataComponent  Load();             // Loads data (returns a reference to the data component)
        string          GetName();          // Gets the component name
        string          GetText();          // Gets the component text
        string          GetAudio();         // Gets the component audio source name 
        string          GetHint();          // Gets the component hint image name 
        string          GetNoise();         // Gets the component noise letters string
        bool            IsChallenge();      // Returns TRUE if the component is a Challenge
        IDataComponent  GetParent();        // Gets the component parent ref.
    }
}
