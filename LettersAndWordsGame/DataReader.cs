﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LettersAndWordsGame
{
    /// <summary>
    /// exs.csv files parser class
    /// </summary>
    class ExercicesFileParser
    {
        private string[] szExsDesc;
        private int nExIndex;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="szExsFile">exs.csv file path</param>
        public ExercicesFileParser(string szExsFile)
        {
            nExIndex = 0;
            if(File.Exists(@szExsFile))
            {
                szExsDesc = File.ReadAllLines(@szExsFile);
            }
        }

        /// <summary>
        /// Makes and returns next Spelling Exercice in exs.csv file
        /// </summary>
        /// <returns>next Spelling Exercice in exs.csv file</returns>
        public SpellingEx MakeNextSpellingEx(Challenge cHeadChlg)
        {
            SpellingEx cSpEx = null;
            if (nExIndex < szExsDesc.Length)
            {
                string szText, szAudio, szNoise, szHint;
                string[] szParts = szExsDesc[nExIndex].Split(new char[]{','}, StringSplitOptions.None);
                int nI = 0;
                szText  = (nI < szParts.Length)? szParts[nI] : ""; nI++;
                szAudio = (nI < szParts.Length)? szParts[nI] : ""; nI++;
                szNoise = (nI < szParts.Length)? szParts[nI] : ""; nI++;
                szHint  = (nI < szParts.Length)? szParts[nI] : ""; 
                cSpEx = new SpellingEx(szText, szAudio, szNoise, szHint, cHeadChlg);
                nExIndex++;
            }
            return cSpEx;
        }
    }

    /// <summary>
    /// Game Data Reader class
    /// </summary>
    class DataReader
    {
        private string szDataPath;
        private Challenge cHeadChallenge;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="szGameDataPath">Game data path</param>
        public DataReader(string szGameDataPath)
        {
            szDataPath = szGameDataPath;
            if (Directory.Exists(@szDataPath))
            {
                string[] lDirs = Directory.GetDirectories(@szDataPath);
                if (lDirs.Length > 0) { cHeadChallenge = new Challenge("TOP CHALLENGE"); }
                if(cHeadChallenge!=null) CreateChallenges(cHeadChallenge, lDirs, @szDataPath);
            }
        }

        /// <summary>
        /// Return resources folder path
        /// </summary>
        /// <returns></returns>
        public string GetRESPath()
        {
            return string.Format("{0}../RES/", szDataPath);
        }

        /// <summary>
        /// Creates the set of sub-challenges / spelling exercices and add them to the passed head challenge
        /// </summary>
        /// <param name="cHeadChlg">head challenge</param>
        /// <param name="lDirs">list of subdirectories</param>
        /// <param name="szHeadChlgPath">head challenge directory (full path)</param>
        private void CreateChallenges(Challenge cHeadChlg, string[] lDirs, string szHeadChlgPath)
        {
            // Create exercices
            string szExFile = String.Format("{0}exs.csv", szHeadChlgPath);
            if (File.Exists(@szExFile)) {
                ExercicesFileParser cParser = new ExercicesFileParser(szExFile);
                SpellingEx cSpEx = cParser.MakeNextSpellingEx(cHeadChlg);
                while (cSpEx != null)
                {
                    cHeadChlg.AddToExs(cSpEx);
                    cSpEx = cParser.MakeNextSpellingEx(cHeadChlg);
                }
            }
            // Create challenges
            foreach (string szDir in lDirs)
            {
                string szDirName = szDir.Substring(szHeadChlgPath.Length);
                Challenge chlg = new Challenge(szDirName, cHeadChlg);
                cHeadChlg.AddToExs(chlg);
                string szChlgDir = String.Format("{0}/", szDir);
                string[] lSubDirs = Directory.GetDirectories(@szChlgDir);
                CreateChallenges(chlg, lSubDirs, szChlgDir);
            }
        }

        /// <summary>
        /// Return true if no data read
        /// </summary>
        /// <returns>true if no data read</returns>
        public bool NoData()
        {
            return (cHeadChallenge == null);
        }

        /// <summary>
        /// Returns the list of active page challenges names
        /// </summary>
        /// <returns>the list of active page challenges names</returns>
        public List<string> GetChallengesNames()
        {
            ResetLoader();
            List<string> lChlgNames = new List<string>();
            IDataComponent cComp = cHeadChallenge.Load();
            while (cComp != null)
            {
                if(cComp.IsChallenge()) lChlgNames.Add(cComp.GetName());
                cHeadChallenge.Advance();
                cComp = cHeadChallenge.Load(); // load next
            }
            return lChlgNames;
        }

        /// <summary>
        /// Selects the challenge given by its name
        /// </summary>
        /// <param name="szChallengeToSelName">name of the challenge to select</param>
        public void SelectChallenge(string szChallengeToSelName)
        {
            ResetLoader();
            IDataComponent cComp = cHeadChallenge.Load();
            while ( (cComp != null) && (cComp.GetName() != szChallengeToSelName) )
            {
                cHeadChallenge.Advance();
                cComp = cHeadChallenge.Load();
            }
            if ( (cComp != null) && (cComp.IsChallenge()) ) { cHeadChallenge = (Challenge)cComp; }
            else                                            { MessageBox.Show("DataReader Error: Selected challenge is invalid!!"); }
        }

        /// <summary>
        /// Selects the parent challenge of the current one; Go backwards
        /// </summary>
        public void SelectParent()
        {
            Challenge cParent = (Challenge)cHeadChallenge.GetParent();
            if (cParent != null)    { cHeadChallenge = cParent; }
        }

        /// <summary>
        /// Check the challenge given by its name is selected
        /// </summary>
        /// <param name="szChlgName">challenge name to check</param>
        /// <returns>TRUE if selected, FALSE otherwise</returns>
        public bool IsSelected(string szChlgName)
        {
            return (cHeadChallenge.GetName() == szChlgName);
        }

        /// <summary>
        /// Checks whether the reader currently points to a challenge that has spelling exercices
        /// </summary>
        /// <returns>TRUE if the reader currently points to a challenge that has spelling exercices</returns>
        public bool HasExs()
        {
            ResetLoader();
            IDataComponent cComp = cHeadChallenge.Load();
            return (cComp != null)? !cComp.IsChallenge() : false; // Just check the first one: Spelling exercices are always listed first!!
        }

        /// <summary>
        /// Resets the selected challenge loader
        /// </summary>
        public void ResetLoader(Button cAudio = null)
        {
            cHeadChallenge.ResetLoader();
            if(cAudio != null) cAudio.Text = "1";
        }

        /// <summary>
        /// Returns full name (with all parents up to root) of currently selected challenge
        /// </summary>
        /// <returns>full name (with all parents up to root) of currently selected challenge</returns>
        public string GetCurrentName()
        {
            string szChlgFullName = ".";
            Challenge cParentChlg = cHeadChallenge;
            while ((cParentChlg != null) && (cParentChlg.GetName() != "TOP CHALLENGE"))
            {
                szChlgFullName = cParentChlg.GetName() + "/" + szChlgFullName;
                cParentChlg = (Challenge)cParentChlg.GetParent();
            }
            return szChlgFullName;
        }

        /// <summary>
        /// Returns current active exercice text 
        /// (if a sub-challenge of the selected challenge is currently active, it returns its active exercice text)
        /// </summary>
        /// <returns>current active exercice text</returns>
        public string GetCurrentText()
        {
            return cHeadChallenge.GetText();
        }

        /// <summary>
        /// Returns current active exercice audio file 
        /// (if a sub-challenge of the selected challenge is currently active, it returns its active exercice audio file)
        /// </summary>
        /// <returns>current active exercice audio file</returns>
        public string GetCurrentAudio()
        {
            Challenge cParentChlg = cHeadChallenge;
            while ((cParentChlg != null) && (cParentChlg.GetParent().GetName() != "TOP CHALLENGE"))
            {
                cParentChlg = (Challenge)cParentChlg.GetParent();
            }
            return string.Format("{0}{1}", szDataPath, cParentChlg.GetAudio());
        }

        /// <summary>
        /// Returns current active exercice hint file 
        /// (if a sub-challenge of the selected challenge is currently active, it returns its active exercice hint file)
        /// </summary>
        /// <returns>current active exercice hint file</returns>
        public string GetCurrentHint()
        {
            Challenge cParentChlg = cHeadChallenge;
            while ((cParentChlg != null) && (cParentChlg.GetParent().GetName() != "TOP CHALLENGE"))
            {
                cParentChlg = (Challenge)cParentChlg.GetParent();
            }
            return string.Format("{0}{1}", szDataPath, cParentChlg.GetHint());
        }

        /// <summary>
        /// Returns current active exercice noise string 
        /// (if a sub-challenge of the selected challenge is currently active, it returns its active exercice noise string)
        /// </summary>
        /// <returns>current active exercice noise string</returns>
        public string GetCurrentNoise()
        {
            string szRawNoise = cHeadChallenge.GetNoise();
            string szNoise = ExpandNoiseStr(szRawNoise);
            return szNoise;
        }

        /// <summary>
        /// Loads the next exercice / challenge
        /// </summary>
        public void LoadNext(Button cAudio)
        {
            cHeadChallenge.Advance();
            cAudio.Text = string.Format("{0}", cHeadChallenge.GetCurrent()+1);
        }

        /// <summary>
        /// Is the current active data component an exercice?
        /// </summary>
        /// <returns>TRUE if the current active data component an exercice</returns>
        public bool IsCurrentEx()
        {
            return (cHeadChallenge.Load()!=null)? !cHeadChallenge.Load().IsChallenge(): false;
        }

        /// <summary>
        /// Expand noise string of type ab[c]-[x]yz
        /// </summary>
        /// <param name="szRawNoise">raw noise string</param>
        /// <returns></returns>
        private string ExpandNoiseStr(string szRawNoise)
        {
            StringBuilder cSzBuilder = new StringBuilder(szRawNoise);
            string lowerCaseLatinLetters = "abcdefghijklmnopqrstuvwxyz";
            string upperCaseLatinLetters = lowerCaseLatinLetters.ToUpper();
            string arabicLetters = "ابتثجحخدذرزسشصضطظعغفقكلمنهوي";
            int nDashPos = szRawNoise.IndexOf('-');
            while( (nDashPos > 0) && (nDashPos < (szRawNoise.Length-1)) )
            {
                char cStart = szRawNoise[nDashPos - 1];
                char cEnd = szRawNoise[nDashPos + 1];
                if ( lowerCaseLatinLetters.Contains(cStart) && lowerCaseLatinLetters.Contains(cEnd) )
                {
                    cSzBuilder.Replace("-", GetRangeFromChars(lowerCaseLatinLetters, cStart, cEnd));
                }
                else if( upperCaseLatinLetters.Contains(cStart) && upperCaseLatinLetters.Contains(cEnd) )
                {
                    cSzBuilder.Replace("-", GetRangeFromChars(upperCaseLatinLetters, cStart, cEnd));
                }
                else if( arabicLetters.Contains(cStart) && arabicLetters.Contains(cEnd) )
                {
                    cSzBuilder.Replace("-", GetRangeFromChars(arabicLetters, cStart, cEnd));
                }
                szRawNoise = szRawNoise.Remove(nDashPos, 1);
                nDashPos = szRawNoise.IndexOf('-');
            }
            return cSzBuilder.ToString();
        }

        /// <summary>
        /// Gets a string within string using its limiting chars without the limits
        /// </summary>
        /// <param name="szStr">input string to extract from</param>
        /// <param name="cStart">start char</param>
        /// <param name="cEnd">end char</param>
        /// <returns></returns>
        private string GetRangeFromChars(string szStr, char cStart, char cEnd)
        {
            int nStartPos = szStr.IndexOf(cStart)+1;
            int nRangeLength = szStr.IndexOf(cEnd) - nStartPos; if (nRangeLength < 0) nRangeLength = 0;
            return szStr.Substring(nStartPos, nRangeLength);
        }
    }
}
