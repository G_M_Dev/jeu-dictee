﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LettersAndWordsGame
{
    /// <summary>
    /// Letter buttons factory class
    /// </summary>
    class LetterPBsFactory : ButtonsFactory
    {
        private static LetterPBsFactory cFactoryInstance = null; /// the current factory instance

        /// <summary>
        /// Default constructor
        /// </summary>
        private LetterPBsFactory() : base() { }

        /// <summary>
        /// Get/create the LetterPBsFactory single instance
        /// </summary>
        /// <returns></returns>
        public static LetterPBsFactory GetPBFactory()
        {
            if (cFactoryInstance == null)
            {
                cFactoryInstance = new LetterPBsFactory();
            }
            return cFactoryInstance;
        }

        /// <summary>
        /// Configure the Button
        /// </summary>
        /// <param name="cPB">button instance</param>
        /// <param name="evHdl">event handler</param>
        /// <param name="ePBType">button type</param>
        protected override void ConfigTheButton(Button cPB, EventHandler evHdl = null, BUTTON_TYPE ePBType = BUTTON_TYPE.eLETTER)
        {
            if (ePBType != BUTTON_TYPE.eLETTER) { return; }
            cPB.Size = new Size(60, 60);                        // size
            cPB.Anchor = AnchorStyles.None;                     // Anchor
            Font cFont = new Font("Arial", 30, FontStyle.Bold);
            cPB.Font = cFont;                                   // font
            cPB.ForeColor = Color.OrangeRed;                    // color
            cPB.Click += evHdl;                                 // event
        }

        /// <summary>
        /// Returns a letter button - and adds it at the right position in list
        /// </summary>
        /// <param name="szPBName">Button name - should be one char representing the letter</param>
        /// <param name="evHdl">event handler</param>
        /// <param name="cPBImage">Button image if any</param>
        /// <param name="ePBType">Button type</param>
        /// <returns>Created button</returns>
        public override Button GetButton(string szPBName, EventHandler evHdl = null, Image cPBImage = null, BUTTON_TYPE ePBType = BUTTON_TYPE.eLETTER)
        {
            Button cPB = lPBsList.Find(PB => PB.Name == szPBName); // check if button exists
            if (cPB == null) // if NOT create it, configure it and add it to the list
            {
                cPB = new Button();
                // Configure it
                if (cPBImage != null)
                {
                    cPB.BackgroundImage = cPBImage;
                    cPB.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else
                {
                    cPB.Text = szPBName;
                }
                cPB.Name = szPBName;
                ConfigTheButton(cPB, evHdl, ePBType);
                cPB.TabIndex = 0;
                cPB.UseVisualStyleBackColor = true;
                // Add it to the list
                lPBsList.Insert(GetPBPosInList(cPB.Name[0]), cPB);
            }
            // Return it
            return cPB;
        }

        /// <summary>
        /// Gets letter button position in list based on its label letter char order
        /// </summary>
        /// <param name="cLetter">button label letter char</param>
        /// <returns></returns>
        private int GetPBPosInList(char cLetter)
        {
            int nPbOrder = 0;
            while ( (nPbOrder < lPBsList.Count) && (lPBsList[nPbOrder].Name[0] < cLetter) ) { nPbOrder++; }
            return nPbOrder;
        }

        /// <summary>
        /// Gets the button position in panel based on its order in letter buttons list
        /// </summary>
        /// <param name="nPbOrder"></param>
        /// <param name="bIsArabicChar"></param>
        /// <returns></returns>
        private Point GetPBPosInPanel(int nPbOrder, bool bIsArabicChar = false)
        {
            int nXStepOrder = (nPbOrder % 7);
            int nX = 140 + (bIsArabicChar? (6 - nXStepOrder) : nXStepOrder) *60;
            int nY = 150 + (nPbOrder / 7)*60;
            return new Point(nX, nY);
        }

        /// <summary>
        /// Positions all letter buttons in the list
        /// </summary>
        public void PositionLetters()
        {
            for(int nPb = 0; nPb < lPBsList.Count; nPb++) {
                bool bIsArabicChar = (lPBsList[nPb].Name[0] >= 0x0600) && (lPBsList[nPb].Name[0] <= 0x06FF);
                lPBsList[nPb].Location = GetPBPosInPanel(nPb, bIsArabicChar);
            }
        }
    }
}
